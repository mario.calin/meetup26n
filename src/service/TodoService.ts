import { TodoDTO, TodoDTOFactory } from '../dto/TodoDTO';
import { Todo } from '../model/Todo';
import { TodoRepository } from '../model/TodoRepository';

export class TodoService {
    private todoRepository: TodoRepository;

    constructor(todoRepository: TodoRepository) {
        this.todoRepository = todoRepository;
    }

    public getTodos(): TodoDTO[] {
        const todos: Todo[] = this.todoRepository.getAllTodos();
        return todos.map(todo => TodoDTOFactory.from(todo));
    }

    public getTodoById(todoId: string): TodoDTO {
        this.todoRepository.getTodoById(todoId);
    }
}
