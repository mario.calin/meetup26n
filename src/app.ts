import { Logger } from '@overnightjs/logger';
import { Server } from '@overnightjs/core';
import { TodoController } from './controller/TodoController';
import { TodoRepository } from './model/TodoRepository';
import { TodoService } from './service/TodoService';

export class Application extends Server {
    constructor() {
        super(true); // setting showLogs to true
        this.setupDependenciesAndControllers();
    }

    setupDependenciesAndControllers() {
        const todoRepo = new TodoRepository();
        const todoService = new TodoService(todoRepo);
        const todoController = new TodoController(todoService);

        super.addControllers([todoController]);
    }

    public start(): void {
        const port = process.env.PORT || 3000;
        this.app.listen(port, () => {
            Logger.Imp('----------------------------------------------');
            Logger.Imp('----------------------------------------------');
            Logger.Imp('Server listening on port: ' + port);
            Logger.Imp('----------------------------------------------');
            Logger.Imp('----------------------------------------------');
            Logger.Imp('Application started');
        });
    }
}
