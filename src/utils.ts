import { uniqueId } from 'lodash';

export function provideRandomId(): string {
    return uniqueId();
}
