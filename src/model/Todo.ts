export type TodoLabel = 'available' | 'inprogress' | 'mergerequest';
export type TodoWeight = 'small' | 'medium' | 'large';

export class Todo {
    public id: string;
    public name: string;
    public description: string;
    public label: TodoLabel;
    public weight: TodoWeight;
}
