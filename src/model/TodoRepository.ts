import { clone, cloneDeep } from 'lodash';
import { Todo } from './Todo';
import { provideRandomId } from '../utils';

export class TodoRepository {
    private todoArray: Todo[];

    constructor() {
        this.todoArray.push(
            {
                id: provideRandomId(),
                name: 'Meetup 26N',
                description: 'Crear proyecto meetup y presentación',
                label: 'inprogress',
                weight: 'medium'
            },
            {
                id: provideRandomId(),
                name: 'Modificación en mesa de programación',
                description: 'Permitir modificar borradores en mesa de programación',
                label: 'inprogress',
                weight: 'large'
            },
            {
                id: provideRandomId(),
                name: 'Linting en Fowrard',
                description: 'Aplicar ESLint, Prettier y SonarLint en ',
                label: 'available',
                weight: 'large'
            }
        );
    }

    getAllTodos(): Todo[] {
        return cloneDeep(this.todoArray);
    }

    getTodoById(todoId: string): Todo | undefined {
        const todo = this.todoArray.find(t => t.id === todoId);
        if (todo) return clone(todo);
        return undefined;
    }
}
