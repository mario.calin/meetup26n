import { Application } from './app';

// Create an application instance
const application = new Application();

// Run the application
application.start();
