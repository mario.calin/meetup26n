import { Todo } from '../model/Todo';

export interface TodoDTO {
    id: string;
    name: string;
    description: string;
    label: string;
    weight: string;
}

export class TodoDTOFactory {
    public static from(todo: Todo): TodoDTO {
        return {
            id: todo.id,
            name: todo.name,
            description: todo.description,
            label: todo.label,
            weight: todo.weight
        };
    }
}
