import { Controller, Get } from '@overnightjs/core';
import { Request, Response } from 'express';
import { TodoDTOFactory } from '../dto/TodoDTO';
import { TodoService } from '../service/TodoService';

@Controller('todos')
export class TodoController {
    private todoService: TodoService;

    constructor(todoService: TodoService) {
        this.todoService = todoService;
    }

    @Get('/')
    public getTodos(req: Request, res: Response) {
        const todos = this.todoService.getTodos();
        return res.json(todos);
    }

    @Get('/:id')
    public getTodoById(req: Request, res: Response) {
        const todoId: string = req.params.id;
        const todo = this.todoService.getTodoById(todoId);

        return res.json(TodoDTOFactory.from(todo));
    }
}
